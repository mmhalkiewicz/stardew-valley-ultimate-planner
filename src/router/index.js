import { createRouter, createWebHistory } from "vue-router";
import HomePage from "../views/HomePage.vue";
import CropsPage from "../views/CropsPage.vue";
import ItemsToShippedPage from "../views/ItemsToShippedPage.vue";
import PerfectionTrackerPage from "../views/PerfectionTrackerPage.vue";
import ObelisksPage from "../views/ObelisksPage.vue";
import GoldenWalnutPage from "../views/GoldenWalnutPage.vue";
import GoldenClockPage from "../views/GoldenClockPage.vue";
import MonsterSlayPage from "../views/MonsterSlayPage.vue";
import FriendshipPage from "../views/FriendshipPage.vue";
import FarmerSkillPage from "../views/FarmerSkillPage.vue";
import StardropsPage from "../views/StardropsPage.vue";
import CookingPage from "../views/CookingPage.vue";
import CraftingPage from "../views/CraftingPage.vue";
import FishPage from "../views/FishPage.vue";

const routes = [
    {
        path: "/",
        name: "Home",
        component: HomePage,
    },
    {
      path: "/list-of-crops",
      component: CropsPage
    },
    {
      path: "/perfection-tracker",
        component: PerfectionTrackerPage,
    },
    {
        path: "/perfection-tracker/items-shipped-page",
        component: ItemsToShippedPage,
    },
    {
        path: "/perfection-tracker/obelisks-page",
        component: ObelisksPage,
    },
    {
        path: "/perfection-tracker/golden-walnuts-page",
        component: GoldenWalnutPage,
    },
    {
        path: "/perfection-tracker/golden-clock-page",
        component: GoldenClockPage
    },
    {
        path: "/perfection-tracker/monster-slayed-page",
        component: MonsterSlayPage,
    },
    {
        path: "/perfection-tracker/friendship-page",
        component: FriendshipPage,
    },
    {
        path: "/perfection-tracker/farmer-skills-page",
        component: FarmerSkillPage,
    },
    {
        path: "/perfection-tracker/stardrops-page",
        component: StardropsPage,
    },
    {
        path: "/perfection-tracker/cooking-page",
        component: CookingPage,
    },
    {
        path: "/perfection-tracker/crafting-items-page",
        component: CraftingPage,
    },
    {
        path: "/perfection-tracker/fish-caught-page",
        component: FishPage,
    },

];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
